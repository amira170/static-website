FROM nginx:1.21.1
LABEL maintener="amira mountaka"
RUN apt-get update -y && apt-get install -y git
WORKDIR /usr/share/nginx/html
RUN rm -Rf ./*
RUN git clone https://github.com/diranetafen/static-website-example.git /usr/share/nginx/html
#EXPOSE 5001
CMD ["nginx", "-g", "daemon off;"]
